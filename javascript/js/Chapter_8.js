jQuery(document).ready(function($){
	// CHange content on page after clicking link 

	$('nav a').on('click', function(e){
		e.preventDefault();    // stop loading new link 

		var url = this.href;	// Get url 

		$('nav a.current').removeClass('current'); //clear current page indicator 
		$(this).addClass('current'); 				// set new page indicator 

		$('#container').remove();					//remove old content
		$('#content').load(url + ' #content').hide().fadeIn().slow(); //new content 
	});	


	// USer Choose t shirt without reloading page 
	$('selector').on('click', function(e){
		e.preventDefault();

		vat queryString = 'vote=' + e.target.id; //<-- click on image so target is img 

		$.get('cart/id', queryString, function(data){ //get data from server 1 url 2 data send to server 3 processing data 
			$('selector').html(data);    //replace html in selector data from server 
		});
	});

	//Submitting form 
	$('register').on('submit', function(e){
		e.preventDefault();
		var details = $('#register').serialize();

		$.post('user/register', details. function(data){
			$('#register')html(data);
		})

	})

	//Get JSON and show currency 

	$('#exchangerates').append('<div id="rates"></div><div id="reload"></div>');


	function loadRates(){
		$.getJSON('data/currencyrates.json')
		.done(function(data){
			var d = new Data();
			var hrs = d.getHours();
			var min = d.getMinutes();
			var msg = '<h2> Exchange rates </h2>';
			$.each(data, function(key, val){
				msg += '<div class="'+ key + '">' + key + ': ' + val + '</div>'; 
			});
			msg += '<br>Last update: ' + hrs + ':' + min + '<br>';
			$('#rates').html(msg);
			}).fail(function(){
				$('#aside').append('Sorry, we cannot load rates');
			}).always(function(){
				var reload = '<a id="refresh" href="#">';
				reload += '<img src="img/refresh.png" alt="refresh" /></a>';
				$('#reload').html(reload);
				$('#refresh').on('click', function(e){
					e.preventDefault();
					loadRates();
				});
			});
		}

	loadRates();



	//AJAX shorthand 
		$('nav a').on('click', function(e){
		e.preventDefault();    // stop loading new link 

		var url = this.href;	// Get url 
		var $content = $('#content');

		$('nav a.current').removeClass('current'); //clear current page indicator 
		$(this).addClass('current'); 				// set new page indicator 

		$('#container').remove();					//remove old content
		$.ajax({
			type: "POST",
			url: url,
			timeout: 2000,
			beforeSand: function(){
				$content.append('<div id="load">LOading</div>');
			},
			complete: function(){
				$('loading').remove();
			},
			success: function(data){
				$content.html( $(data).find('#container')).hide().fadeIn(400);
			}
			fail: function(){
				$('#panel').html('<div class="loading">Please try again soon.</div>')
			}
		});//new content 
	});	
});