var name = "Pavel";
var l_name = "Samkov";

var country = 'Poland';
var city = 'Lublin';
var street= 'Kek';
var local = 9;

var f_item_name = 'Dark Side';
var f_item_price = 80;

var s_item_name = 'Black Burn';
var s_item_price = 100;

var first_name = document.getElementById('name');
first_name.textContent = name;

var last_name = document.getElementById('last_name');
last_name.textContent = l_name;

var address = document.getElementById('shipping');
address.textContent = country + ', ' + city + ', ' + street + ' ' + local;

var item = document.getElementById('first_item');
item.textContent = f_item_name;

var item_p = document.getElementById('first_item_cost');
item_p.textContent = f_item_price;

var item = document.getElementById('second_item');
item.textContent = s_item_name;

var item_p = document.getElementById('second_item_cost');
item_p.textContent = s_item_price;

var total = document.getElementById('total');
total.textContent = s_item_price + f_item_price;