//alert(' Current page : ' + window.location);
(function() {

    var hotel = {
        name: 'Park',
        roomRate: 240, 
        discount: 15, 
        offerPrice: function() {
            var offerRate = this.roomRate * ((100 - this.discount) / 100);
            return offerRate;
        }
    };

    var hotelName = document.getElementById('hotelName');
    var roomRate = document.getElementById('roomRate');
    var specialRate = document.getElementById('specialRate');

    hotelName.textContent = hotel.name;
    roomRate.textContent = hotel.roomRate.toFixed(2) + 'PLN';
    specialRate.textContent = hotel.offerPrice() + 'PLN';

    //PART TWO 

    var expiryMsg, today, elEnds;

    function offerExpires(today){
        var weekFromToday, day, date, month, year, dayNames, monthNames;
        weekFromToday = new Date(today.getTime() + 7 * 24 * 60 * 60 * 1000);

        dayNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        day = dayNames[weekFromToday.getDay()];
        date = weekFromToday.getDate();
        month = monthNames[today.getMonth()];
        year = today.getFullYear();

        expiryMsg = 'Offer expires next ';
        expiryMsg += day + '<br /> (' + date + ' ' + month + ' ' + year + ')';
        return expiryMsg;
    }
    today = new Date();

    elEnds = document.getElementById('offerEnds');
    elEnds.innerHTML = offerExpires(today);
}());   